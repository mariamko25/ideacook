import React from 'react'
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native'

/**
 * Classe qui gère l'affichage d'une recette dans une liste
 */
class RecipeItem extends React.Component {

    /**
     * Fonction qui affiche un coeur plein si la recette est en favorite sinon un coeur vide
     * @returns {*}
     * @private
     */
    _displayFavoriteImage() {
        if (this.props.isRecipeFavorite) {
            return (
                <Image
                    style={styles.favorite_image}
                    source={require('../Images/img_favorite.png')}
                />
            )
        } else {
            return (
                <Image
                    style={styles.favorite_image}
                    source={require('../Images/img_non_favorite.png')}
                />
            )
        }
    }

    render() {
        const {recipe, displayDetailsForRecipe} = this.props
        let image = "image"
        if (recipe.thumbnail.trim() !== "") {
            image = recipe.thumbnail;
        }
        return (
            <View style={styles.main_container}>
                <Image
                    style={styles.image}
                    source={{uri: image}}
                />
                <TouchableOpacity onPress={() => displayDetailsForRecipe(recipe)}>
                    <View style={styles.content_container}>
                        <View style={styles.header_container}>
                            {this._displayFavoriteImage()}
                            <Text style={styles.label_text}>{recipe.title}</Text>
                        </View>
                        <View style={styles.description_container}>
                            <Text style={styles.description_text} numberOfLines={6}>{recipe.ingredients}</Text>
                        </View>
                    </View>
                </TouchableOpacity>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        height: 190,
        flexDirection: 'row'
    },
    image: {
        width: 120,
        height: 180,
        margin: 5,
        backgroundColor: 'gray'
    },
    content_container: {
        flex: 1,
        margin: 5
    },
    header_container: {
        flex: 3,
        flexDirection: 'row'
    },
    label_text: {
        fontWeight: 'bold',
        fontSize: 20,
        flex: 1,
        flexWrap: 'wrap',
        paddingRight: 5
    },
    calories_text: {
        fontWeight: 'bold',
        fontSize: 26,
        color: '#666666'
    },
    description_container: {
        flex: 7
    },
    description_text: {
        fontStyle: 'italic',
        color: '#666666'
    },
    date_container: {
        flex: 1
    },
    favorite_image: {
        width: 25,
        height: 25,
        marginRight: 5
    }
})

export default RecipeItem
