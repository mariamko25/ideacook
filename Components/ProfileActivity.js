import React, { Component } from 'react';

import { StyleSheet, View, Button, Text} from 'react-native';
import LoginActivity from "./LoginActivity";


/**
 * Classe qui gère la page du profil de l'utilisateur
 */
class ProfileActivity extends Component {

    constructor(props) {
        super(props);
    }

    render()
    {
        const {goBack} = this.props.navigation;

        if (this.props.navigation.state.params != undefined) {
            return (
                <View style={styles.MainContainer}>

                    <Text style={styles.TextComponentStyle}> {this.props.navigation.state.params.Email} </Text>

                    <Button title="Go back to search" onPress={() => {
                        this.props.navigation.navigate('Search');
                    }}/>
                    <Button title="Click here to Logout" onPress={() => goBack(null)}/>

                </View>
            );
        }else{
            return (
                this.props.navigation.navigate("LoginActivity")
            )
        }
    }
}

const styles = StyleSheet.create({

    MainContainer :{

        justifyContent: 'center',
        flex:1,
        margin: 10,
    },

    TextInputStyleClass: {

        textAlign: 'center',
        marginBottom: 7,
        height: 40,
        borderWidth: 1,
        borderColor: '#2196F3',
        borderRadius: 5 ,

    },

    TextComponentStyle: {
        fontSize: 20,
        color: "#000",
        textAlign: 'center',
        marginBottom: 15
    }
});

export default ProfileActivity