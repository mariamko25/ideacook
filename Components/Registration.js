import {Alert, Button, StyleSheet, Text, TextInput, View} from "react-native";
import React from "react";
import Search from "./Search";

/**
 * Classe qui gère l'inscription d'un utilisateur
 */
class Registration extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            UserName: '',
            UserEmail: '',
            UserPassword: ''
        }
    }

    /**
     * Fonction qui permet la connection avec la base de données
     * @constructor
     */
    UserRegistrationFunction = () => {
        fetch('http://10.188.212.58/user_project/user_registration.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: this.state.UserName,
                email: this.state.UserEmail,
                password: this.state.UserPassword
            })

        }).then((response) => response.json())
            .then((responseJson) => {

                // Showing response message coming from server after inserting records.
                Alert.alert(responseJson);
                if (responseJson.toString() === "User Registered Successfully") {
                    this.props.navigation.navigate("Search")
                }

            }).catch((error) => {
            console.error(error);
            console.log("error", error)
        });

    }

    render() {
        return (
            <View style={styles.MainContainer}>
                <Text style={styles.title}>User Registration Form</Text>
                <TextInput
                    placeholder="Enter User Name"
                    onChangeText={name => this.setState({UserName: name})}
                    underlineColorAndroid='transparent'
                    style={styles.TextInputStyleClass}
                />
                <TextInput
                    placeholder="Enter User Email"
                    onChangeText={email => this.setState({UserEmail: email})}
                    underlineColorAndroid='transparent'
                    style={styles.TextInputStyleClass}
                />
                <TextInput
                    placeholder="Enter User Password"
                    onChangeText={password => this.setState({UserPassword: password})}
                    underlineColorAndroid='transparent'
                    style={styles.TextInputStyleClass}
                    secureTextEntry={true}
                />
                <Button title="Click Here To Register" onPress={this.UserRegistrationFunction} color="#2196F3"/>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    MainContainer: {

        justifyContent: 'center',
        flex: 1,
        margin: 10
    },

    TextInputStyleClass: {

        textAlign: 'center',
        marginBottom: 7,
        height: 40,
        borderWidth: 1,
        borderColor: '#2196F3',
        borderRadius: 5,
    },

    title: {

        fontSize: 22,
        color: "#009688",
        textAlign: 'center',
        marginBottom: 15
    }

})

export default Registration