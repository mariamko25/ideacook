import React from 'react'
import {StyleSheet, View, TextInput, Button, Text, FlatList, ActivityIndicator} from 'react-native'
import RecipeList from './RecipeList'
import {getRecipesFromApiWithSearchedText} from '../API/RecipeAPI'
import {connect} from 'react-redux'

/**
 * Classe qui gère la page de recherche
 */
class Search extends React.Component {

    constructor(props) {
        super(props)
        this.searchedText = ""
        this.currentPage = 0
        this.page = 0
        this.totalPages = 5
        this.state = {
            recipes: [],
            isLoading: false // Par défaut à false car il n'y a pas de chargement tant qu'on ne lance pas de recherche
        }
        this._loadRecipes = this._loadRecipes.bind(this)
    }

    /**
     * Fonction qui est appelé quand le component est rendu
     */
    componentWillMount() {
        getRecipesFromApiWithSearchedText("", "popular", 1).then(data => {
            this.page = this.currentPage + 1
            this.currentPage = this.currentPage + 1
            this.totalPages = 5
            this.setState({
                recipes: [...this.state.recipes, ...data.results],
                isLoading: false
            })
        })
    }

    /**
     * Fonction qui cherche les recettes
     * @private
     */
    _searchRecipes() {
        this.page = 0
        this.currentPage = 0
        this.totalPages = 5
        this.setState({
            recipes: [],
        }, () => {
            this._loadRecipes()
        })
    }

    /**
     * Fonction qui affiche le chargement d'une page
     * @returns {*}
     * @private
     */
    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size='large'/>
                </View>
            )
        }
    }

    /**
     * Fonction qui récupère le texte tapé par l'utilisateur
     * @param text
     * @private
     */
    _searchTextInputChanged(text) {
        this.searchedText = text
    }


    /**
     * Fonction qui charge les recettes
     * @private
     */
    _loadRecipes() {
        if (this.searchedText.length > 0) {
            this.setState({isLoading: true})
            getRecipesFromApiWithSearchedText("", this.searchedText, this.page + 1).then(data => {
                this.page = this.currentPage + 1
                this.currentPage = this.currentPage + 1
                this.totalPages = 5
                this.setState({
                    recipes: [...this.state.recipes, ...data.results],
                    isLoading: false
                })
            })
        } else {
            this.setState({isLoading: true})
            getRecipesFromApiWithSearchedText("", "popular", 1).then(data => {
                this.page = this.currentPage + 1
                this.currentPage = this.currentPage + 1
                this.totalPages = 5
                this.state.recipes = []
                this.setState({
                    recipes: [...this.state.recipes, ...data.results],
                    isLoading: false
                })
            })
        }
    }


    render() {
        return (

            <View style={styles.main_container}>
                <TextInput
                    style={styles.textinput}
                    placeholder='Recipe title'
                    onChangeText={(text) => this._searchTextInputChanged(text)}
                    onSubmitEditing={() => this._searchRecipes()}
                />
                <Button title='Search' onPress={() => this._searchRecipes()}/>
                <RecipeList
                    recipes={this.state.recipes}
                    navigation={this.props.navigation}
                    loadRecipes={this._loadRecipes}
                    page={this.page}
                    totalPages={this.totalPages}
                    favoriteList={false} // Ici j'ai simplement ajouté un booléen à false pour indiquer qu'on n'est pas dans le cas de l'affichage de la liste des films favoris. Et ainsi pouvoir déclencher le chargement de plus de films lorsque l'utilisateur scrolle.
                />
                {this._displayLoading()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1

    },
    textinput: {
        marginLeft: 5,
        marginRight: 5,
        height: 50,
        borderColor: '#000000',
        borderWidth: 1,
        paddingLeft: 5
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

/**
 * Fonction qui mappe la liste des recettes favorites avec les props de notre component
 * @param state
 * @returns {{favoritesRecipe: Array}}
 */
const mapStateToProps = state => {
    return {
        favoritesRecipe: state.favoritesRecipe
    }
}

export default connect(mapStateToProps)(Search)
