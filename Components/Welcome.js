import React from 'react'
import {View, Button, ImageBackground, Text, StyleSheet, TouchableOpacity, AsyncStorage} from 'react-native'

/***
 * Classe qui gère la page d'acceuil de l'application
 */
class Welcome extends React.Component {

    constructor(props) {
        super(props)
        this._storeData()
    }

    /**
     * Fonction qui permet d'initialiser l'utilisateur à null
     * @returns {Promise<void>}
     * @private
     */
    _storeData = async () => {
        try {
            await AsyncStorage.setItem('userEmail', 'null');
        } catch (error) {
            // Error saving data
        }
    };

    /**
     * Fonction qui permet de passer à la page de recherche
     * @param text
     * @private
     */
    _displaySearch = (text) => {
        this.props.navigation.navigate("Search", {text: text})
    }

    render() {
        return (
            <View>
                <ImageBackground source={require('../Images/image.png')} style={{width: '100%', height: '100%'}}>
                    <TouchableOpacity onPress={() => this._displaySearch('popular')}>
                        <Text style={styles.loading_container}>
                            Discover
                        </Text>
                    </TouchableOpacity>
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
    },
    loading_container: {
        position: 'relative',
        marginTop: 550,
        marginLeft: 160,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: 'black',
        textAlign: 'center',
        backgroundColor: 'black',
        color: 'white',
        width: '20%',
    },
    scrollview_container: {
        flex: 1
    },
    image: {
        height: 169,
        margin: 5
    },
    title_text: {
        fontWeight: 'bold',
        fontSize: 35,
        flex: 1,
        flexWrap: 'wrap',
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        marginBottom: 10,
        color: '#000000',
        textAlign: 'center'
    },
    description_text: {
        fontStyle: 'italic',
        color: '#666666',
        margin: 5,
        marginBottom: 15
    },
    default_text: {
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5,
        fontSize: 15
    }
})
export default Welcome
