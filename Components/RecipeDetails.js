import React from 'react'
import {
    StyleSheet,
    View,
    Text,
    ActivityIndicator,
    ScrollView,
    Image,
    TouchableOpacity,
    AsyncStorage,
} from 'react-native'
import Hyperlink from 'react-native-hyperlink'
import {connect} from 'react-redux'

/**
 * Classe qui gère la page affichant les détails d'une recette
 */
class RecipeDetails extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            recipe: undefined,
            isLoading: false
        }
    }

    /**
     * Fonction qui affiche le chargement d'une page
     * @returns {*}
     * @private
     */
    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size='large'/>
                </View>
            )
        }
    }

    /**
     * Fonction qui est appelée quand le component a fini d'être rendu
     */
    componentDidMount() {
        const favoritesRecipeIndex = this.props.favoritesRecipe.findIndex(item => item.href === this.props.navigation.state.params.recipe.href)

        // Film déjà dans nos favoris, on a déjà son détail
        if (favoritesRecipeIndex !== -1) {
            this.setState({
                recipe: this.props.favoritesRecipe[favoritesRecipeIndex],
                isLoading: false
            })
            return
        }

        this.setState({isLoading: true})
        this.setState({
            recipe: this.props.navigation.state.params.recipe,
            isLoading: false
        })

    }

    /**
     * Fonction appelé quand le component est mis à jour
     */
    componentDidUpdate() {
        console.log("componentDidUpdate : ")
        console.log(this.props.favoritesRecipe)
    }

    /**
     * Fonction qui affiche un coeur plein quand la recette est en favoris sinon un coeur vide
     * @returns {*}
     * @private
     */
    _displayFavoriteImage() {
        var sourceImage = require('../Images/img_non_favorite.png')
        console.log(this.state.recipe.href);
        if (this.props.favoritesRecipe.findIndex(item => item.href === this.state.recipe.href) !== -1) {
            // Film dans nos favoris
            sourceImage = require('../Images/img_favorite.png')
        }
        return (
            <Image
                style={styles.favorite_image}
                source={sourceImage}
            />
        )
    }

    /**
     * Fonction qui permet de récupérer des données depuis la base de données
     * @returns {Promise<void>}
     * @private
     */
    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('userEmail');
            if (value !== null) {
                console.log(value);
                if (this.props.favoritesRecipe.findIndex(item => item.href === this.state.recipe.href) !== -1) {
                    this._addFavoriteRecipeFunction(value);
                } else {
                    this._removeFavoriteRecipeFunction(value);
                }
            }
        } catch (error) {
            // Error retrieving data
        }
    };

    /**
     * Fonction qui permet d'ajouter une recette dans la base de données
     * @param email
     * @private
     */
    _addFavoriteRecipeFunction(email) {
        if (email !== undefined) {
            fetch('http://10.188.212.58/user_project/add_favorite.php', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    href: this.state.recipe.href,
                    ingredients: this.state.recipe.ingredients,
                    thumbnail: this.state.recipe.thumbnail,
                    title: this.state.recipe.title,
                    email: email,
                })

            }).then((response) => response.json())
                .then((responseJson) => {
                    // Showing response message coming from server after inserting records.
                    //Alert.alert(responseJson);

                }).catch((error) => {
                console.error(error);
                console.log("error", error)
            });
        }
    }

    /**
     * Fonction qui permet de supprimer une recette de la base de données
     * @param email
     * @private
     */
    _removeFavoriteRecipeFunction(email) {
        console.log(email);
        if (email !== undefined) {
            fetch('http://10.188.212.58/user_project/remove_favorite.php', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    href: this.state.recipe.href,
                    email: email,
                })

            }).then((response) => response.json())
                .then((responseJson) => {
                    // Showing response message coming from server after inserting records.
                    //Alert.alert(responseJson);

                }).catch((error) => {
                console.error(error);
                console.log("error", error)
            });
        }
    }

    /**
     * Fonction qui appelle le reducer
     * @private
     */
    _toggleFavorite() {
        const action = {type: "TOGGLE_FAVORITE", value: this.state.recipe}
        this.props.dispatch(action)

    }

    /**
     * Fonction d'affichage du détail de la recette
     * @returns {*}
     * @private
     */
    _displayRecipe() {

        if (this.state.recipe != undefined) {
            let image = "image"
            if (this.state.recipe.thumbnail.trim() !== "") {
                image = this.state.recipe.thumbnail;
            }
            return (
                <ScrollView style={styles.scrollview_container}>

                    <Image
                        style={styles.image}
                        source={{uri: image}}
                    />

                    <Text style={styles.title_text}>{this.state.recipe.title}</Text>
                    <TouchableOpacity
                        style={styles.favorite_container}
                        onPress={() => {
                            this._toggleFavorite();
                            this._retrieveData();
                        }}>
                        {this._displayFavoriteImage()}
                    </TouchableOpacity>
                    <Text style={styles.description_text} numberOfLines={6}>{this.state.recipe.ingredients}</Text>
                    <Hyperlink
                        linkDefault={true}
                        linkStyle={{color: 'brown'}}
                    >
                        <Text style={styles.default_text} numberOfLines={6}>Click on {this.state.recipe.href} to see the
                            full recipe</Text>
                    </Hyperlink>
                </ScrollView>
            )
        }

    }

    render() {
        return (
            <View style={styles.main_container}>
                {this._displayLoading()}
                {this._displayRecipe()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    scrollview_container: {
        flex: 1
    },
    image: {
        height: 169,
        margin: 5
    },
    title_text: {
        fontWeight: 'bold',
        fontSize: 35,
        flex: 1,
        flexWrap: 'wrap',
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        marginBottom: 10,
        color: '#000000',
        textAlign: 'center'
    },
    description_text: {
        fontStyle: 'italic',
        color: '#666666',
        margin: 5,
        marginBottom: 15
    },
    default_text: {
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5,
        fontSize: 15
    },
    favorite_container: {
        alignItems: 'center',
    },
    favorite_image: {
        width: 40,
        height: 40
    }
})

/**
 * Fonction qui mappe la liste des recettes favorites aux props de notre component
 * @param state
 * @returns {{favoritesRecipe: Array}}
 */
const mapStateToProps = (state) => {
    return {
        favoritesRecipe: state.toggleFavorite.favoritesRecipe
    }
}

export default connect(mapStateToProps)(RecipeDetails)
