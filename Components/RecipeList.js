// Components/RecipeList.js
import React from 'react'
import {StyleSheet, FlatList, AsyncStorage, Alert} from 'react-native'
import RecipeItem from './RecipeItem'
import {connect} from 'react-redux'

/**
 * Classe qui gère la liste des recettes
 */
class RecipeList extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            recipes: []
        }

    }

    /**
     * Fonction qui dirige l'utilisateur vers la page de détail de la recette sélectionnée
     * @param recipe
     * @private
     */
    _displayDetailsForRecipe = (recipe) => {
        this.props.navigation.navigate('RecipeDetails', {recipe: recipe})
    }

    render() {
        return (
            <FlatList
                style={styles.list}
                data={this.props.recipes}
                extraData={this.props.favoritesRecipe}
                keyExtractor={(item) => item.href}
                renderItem={({item}) => (
                    <RecipeItem
                        recipe={item}
                        isRecipeFavorite={(this.props.favoritesRecipe.findIndex(film => film.href === item.href) !== -1) ? true : false}
                        displayDetailsForRecipe={this._displayDetailsForRecipe}
                    />
                )}
                onEndReachedThreshold={0.5}
                onEndReached={() => {
                    if (!this.props.favoritesRecipe && this.props.page < this.props.totalPages) {
                        this.props.loadRecipes()
                    }
                }}
            />
        )
    }
}

const styles = StyleSheet.create({
    list: {
        flex: 1
    }
})

/**
 * Fonction qui mappe la liste des recettes favorites avec les props de notre component
 * @param state
 * @returns {{favoritesRecipe: Array}}
 */
const mapStateToProps = state => {
    return {
        favoritesRecipe: state.toggleFavorite.favoritesRecipe
    }
}

export default connect(mapStateToProps)(RecipeList)
