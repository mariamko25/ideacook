import React from 'react'
import {StyleSheet, View} from 'react-native'
import RecipeList from './RecipeList'
import {connect} from 'react-redux'
import Avatar from './Avatar'

/**
 * Classe qui gère la page des favoris
 */
class Favorites extends React.Component {

    render() {
        return (
            <View style={styles.main_container}>
                <View style={styles.avatar_container}>
                    <Avatar/>
                </View>

                <RecipeList
                    recipes={this.props.favoritesRecipe}
                    navigation={this.props.navigation}
                    favoriteList={true} // Ici on est bien dans le cas de la liste des films favoris. Ce booléen à true permettra d'empêcher de lancer la recherche de plus de films après un scroll lorsqu'on est sur la vue Favoris.
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1
    },
    avatar_container: {
        alignItems: 'center'
    }
})

/**
 * Fonction qui notre liste de recettes favorites aux props de notre component
 */
const mapStateToProps = state => {
    return {
        favoritesRecipe: state.toggleFavorite.favoritesRecipe
    }
}

export default connect(mapStateToProps)(Favorites)
