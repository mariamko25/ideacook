/**
 * Fonction permettant d'obtenir la recette à partir d'ingrédients et de mots clés
 */
export function getRecipesFromApiWithSearchedText (ingredients,text,page) {
  const url= 'http://www.recipepuppy.com/api/?i='+ingredients+'&q='+text+'&p='+page
  return fetch(url)
   .then((response) => response.json())
   .catch((error) => console.error(error))
}
