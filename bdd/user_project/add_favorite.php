<?php
 
// Importing DBConfig.php file.
include 'dbconfig.php';
 
// Creating connection.
$con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
 
// Getting the received JSON into $json variable.
$json = file_get_contents('php://input');
 
// decoding the received JSON and store into $obj variable.
$obj = json_decode($json,true);
 
 // Populate href from JSON $obj array and store into $href.
$href = $obj['href'];
$ingredients = $obj['ingredients'];
$thumbnail = $obj['thumbnail'];
$title = $obj['title'];

 // Populate User email from JSON $obj array and store into $email.
$email = $obj['email'];
 
//Checking name/email already exist or not using SQL query.
$CheckSQL_href = "SELECT * FROM recipes WHERE href='$href'";
$check_href = mysqli_fetch_array(mysqli_query($con,$CheckSQL_href));
 
 if(isset($check_href)){
	 
	// Converting the message into JSON format.
	$idRecipe = $check_href[0];
	
}else{

	// Creating SQL query and insert the record into MySQL database table.
	$Sql_Query = "insert into recipes (href, thumbnail, ingredients, title) values ('$href', '$thumbnail', '$ingredients', '$title')";

	if(mysqli_query($con,$Sql_Query)){
		// If the record inserted successfully then show the message.
		$MSG = 'Favorite Recipe Registered Successfully' ;
		
		$CheckSQL_href = "SELECT * FROM recipes WHERE href='$href'";
		$check_href = mysqli_fetch_array(mysqli_query($con,$CheckSQL_href));
		
		$idRecipe = $check_href[0];
	}
}
 
$CheckSQL_user = "SELECT * FROM user_details WHERE email LIKE '$email'";
$check_user = mysqli_fetch_array(mysqli_query($con,$CheckSQL_user));

 if(isset($check_user)){
	 
	$idUser = $check_user[0];
	
	$Sql_Query = "insert into recipes_favorites (id_recipe, id_user) values ('$idRecipe', '$idUser')";
	
	if(mysqli_query($con,$Sql_Query)){
 
		// If the record inserted successfully then show the message.
		$MSG = 'Favorite Recipe Registered Successfully' ;

		// Converting the message into JSON format.
		$json = json_encode($MSG);

		// Echo the message.
		echo $json ;

	}
}else{
	$MSG = "Pas d'utilisateur";

	// Converting the message into JSON format.
	$json = json_encode($MSG);

	// Echo the message.
	echo $json ;
}
 
 mysqli_close($con);
?>