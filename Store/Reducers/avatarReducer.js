const initialState = { avatar: require('../../Images/img_tag_faces.png') }

/**
 * Fonction qui permet de changer le state concernant l'avatar
 * @param state
 * @param action
 * @returns {*}
 */
function setAvatar(state = initialState, action) {
    let nextState
    switch (action.type) {
        case 'SET_AVATAR':
            nextState = {
                ...state,
                avatar: action.value
            }
            return nextState || state
        default:
            return state
    }
}

export default setAvatar