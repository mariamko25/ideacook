const initialState = { favoritesRecipe: [] }

/**
 * Fonction qui permet de changer le state de la liste des recettes favorites
 * @param state
 * @param action
 * @returns {*}
 */
function toggleFavorite(state = initialState, action) {
  let nextState
  switch (action.type) {
    case 'TOGGLE_FAVORITE':
      const favoritesRecipeIndex = state.favoritesRecipe.findIndex(item => item.href === action.value.href)
      if (favoritesRecipeIndex !== -1) {
        // Le Recipe est déjà dans les favoris, on le supprime de la liste
        nextState = {
          ...state,
          favoritesRecipe: state.favoritesRecipe.filter( (item, index) => index !== favoritesRecipeIndex)
        }
      }
      else {
        // Le Recipe n'est pas dans les Recipe favoris, on l'ajoute à la liste
        nextState = {
          ...state,
          favoritesRecipe: [...state.favoritesRecipe, action.value]
        }
      }
      return nextState || state
  default:
    return state
  }
}

export default toggleFavorite
