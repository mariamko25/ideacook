import { createStore } from 'redux';
import toggleFavorite from './Reducers/favoriteReducer'
import { persistCombineReducers } from "redux-persist";
import storage from 'redux-persist/lib/storage'
import setAvatar from './Reducers/avatarReducer'

/***
 * Configuration pour le stockage
 * @type {{storage: WebStorage, key: string}}
 */
const rootPersistConfig = {
    key: 'root',
    storage: storage
}

export default createStore(persistCombineReducers(rootPersistConfig,{toggleFavorite, setAvatar}))
