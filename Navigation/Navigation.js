import React from 'react'
import {StyleSheet, Image} from 'react-native';
import {createStackNavigator, createAppContainer, createBottomTabNavigator} from 'react-navigation'
import Search from '../Components/Search'
import RecipeDetails from '../Components/RecipeDetails'
import Welcome from '../Components/Welcome'
import Favorites from '../Components/Favorites'
import Registration from "../Components/Registration";
import LoginActivity from "../Components/LoginActivity";
import ProfileActivity from "../Components/ProfileActivity";


/**
 * Fonction qui gère l'empilement des pages de l'onglet recherche
 * @type {NavigationContainer}
 */
const SearchStackNavigator = createStackNavigator({

    Search: { // Ici j'ai appelé la vue "Search" mais on peut mettre ce que l'on veut. C'est le nom qu'on utilisera pour appeler cette vue
        screen: Search,
        navigationOptions: ({navigation}) => ({
            title: 'Search',
            //headerRight: <Button title='Profile' onPress={() => navigation.navigate("ProfileActivity")}/>
        })
    },
    RecipeDetails: { // Encore une fois j'ai mis le même nom que celui du component mais libre à vous de choisir un nom différent
        screen: RecipeDetails
    },
    Registration: {
        screen: Registration,
        navigationOptions: {
            title: 'Registration'
        }
    }
})

/**
 * Fonction qui gère l'empilement des pages de l'onglet Favoris
 * @type {NavigationContainer}
 */
const FavoritesStackNavigator = createStackNavigator({
    Favorites: {
        screen: Favorites,
        navigationOptions: ({navigation}) => ({
            title: 'Favorites',
            //headerRight: <Button title='Profile' onPress={() => navigation.navigate("ProfileActivity")}/>
        })
    },
    RecipeDetails: { // Encore une fois j'ai mis le même nom que celui du component mais libre à vous de choisir un nom différent
        screen: RecipeDetails
    }
})

/**
 * Fonction qui gère l'empilement des pages de l'onglet d'acceuil
 * @type {NavigationContainer}
 */
const HomeStackNavigator = createStackNavigator({
    Welcome: { // Ici j'ai appelé la vue "Search" mais on peut mettre ce que l'on veut. C'est le nom qu'on utilisera pour appeler cette vue
        screen: Welcome,
        navigationOptions: ({navigation}) => ({
            title: 'Welcome',
            //headerRight: <Button title='Login' onPress={() => navigation.navigate("LoginActivity")}/>
        })
    },
    LoginActivity: { // Encore une fois j'ai mis le même nom que celui du component mais libre à vous de choisir un nom différent
        screen: LoginActivity,
    },
    ProfileActivity: {
        screen: ProfileActivity,
    }
})

/**
 * Fonction qui gère les différents onglets (accueil, recherche, favoris)
 * @type {NavigationContainer}
 */
const RecipesTabNavigator = createBottomTabNavigator({
        Welcome: { // Ici j'ai appelé la vue "Search" mais on peut mettre ce que l'on veut. C'est le nom qu'on utilisera pour appeler cette vue
            screen: HomeStackNavigator,
            navigationOptions: {
                tabBarIcon: () => { // On définit le rendu de nos icônes par les images récemment ajoutés au projet
                    return <Image
                        source={require('../Images/img_home.png')}
                        style={styles.icon}/> // On applique un style pour les redimensionner comme il faut
                }
            }
        },
        Search: {
            screen: SearchStackNavigator,
            navigationOptions: {
                tabBarIcon: () => { // On définit le rendu de nos icônes par les images récemment ajoutés au projet
                    return <Image
                        source={require('../Images/img_search.png')}
                        style={styles.icon}/> // On applique un style pour les redimensionner comme il faut
                }
            }
        },
        Favorites: {
            screen: FavoritesStackNavigator,
            navigationOptions: {
                tabBarIcon: () => { // On définit le rendu de nos icônes par les images récemment ajoutés au projet
                    return <Image
                        source={require('../Images/img_favorite.png')}
                        style={styles.icon}/> // On applique un style pour les redimensionner comme il faut
                }
            }
        }
    },
    {
        tabBarOptions: {
            showLabel: false, // On masque les titres
            showIcon: true // On informe le TabNavigator qu'on souhaite afficher les icônes définis
        }
    }
)

const styles = StyleSheet.create({
    icon: {
        width: 30,
        height: 30
    }
})

export default createAppContainer(RecipesTabNavigator)
